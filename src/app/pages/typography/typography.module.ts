import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing }       from './typography.routing';
import { DropdownModule, ModalModule } from 'ng2-bootstrap/ng2-bootstrap';

import { Typography } from './typography.component';
import { Headers } from './components/headers/headers.component';
// import { CodeSample } from 'theme/components/code-sample/code-sample.component';

// see more @ https://github.com/seiyria/ng2-ace
import { AceEditorDirective } from 'ng2-ace';
import 'brace/theme/monokai';
import 'brace/mode/html';

import { sgCodeEx } from 'theme/components/sgCodeEx/sgCodeEx.component'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    DropdownModule,
    ModalModule,
    routing
  ],
  declarations: [
    Typography,
    Headers,
    // CodeSample
    AceEditorDirective,
    sgCodeEx
  ],
  providers: [

  ]
})
export default class TypographyModule {
}
