import { Routes, RouterModule }  from '@angular/router';

import { Typography } from './typography.component';
import { Headers } from './components/headers/headers.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Typography,
    children: [
      { path: 'headers', component: Headers },
      
    ]
  }
];

export const routing = RouterModule.forChild(routes);
