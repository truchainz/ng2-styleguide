import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'headers',
  styles: [require('./headers.scss')],
  template: require('./headers.html'),
})
export class Headers {

    public text: string = "";
    public onChange;

  constructor() {
      this.onChange = (data) => {
      console.log(data);
    }
  }
}
