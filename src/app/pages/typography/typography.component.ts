import {Component} from '@angular/core';

@Component({
  selector: 'typography',
  styles: [],
  template: `<router-outlet></router-outlet>`
})
export class Typography {

  constructor() {
  }
}
