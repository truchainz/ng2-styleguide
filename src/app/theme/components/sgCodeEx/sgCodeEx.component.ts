import {Component, Input} from '@angular/core';

@Component({
  selector: 'sgCodeEx',
  template: `
    <div ace-editor
             [text]="text"
             [mode]="'html'"
             [theme]="'monokai'"
             [options]="{ printMargin: false }"
             [readOnly]="true"
             (textChanged)="onChange($event)"
             style="display:block; height: 10vh; width:100%"></div>
  `
})
export class sgCodeEx {

  @Input() text: string;

  constructor() {
    
  }

}
